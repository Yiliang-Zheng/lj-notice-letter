﻿using Data.Entity;

namespace Data
{
    public class ConfigurationSetting
    {
        public string ConnectionString { get; set; }

        public string SesUsername { get; set; }

        public string SesPassword { get; set; }
    }
}