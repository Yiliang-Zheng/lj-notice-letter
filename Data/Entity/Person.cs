﻿using System;
using Dapper.Contrib.Extensions;

namespace Data.Entity
{
    [Table("sec_Person")]
    public class Person:EntityBase
    {
        [Column(Name = "PersonId")]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Column(Name = "PersonGuid")]
        public Guid Guid { get; set; }
    }
}