﻿using Data.Entity;
using Data.Infrastructure;
using Data.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Data.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDatabaseColumnMapper(this IServiceCollection services)
        {
            Dapper.SqlMapper.SetTypeMap(typeof(Person), new ColumnAttributeTypeMapper<Person>());
            return services;
        }

        public static IServiceCollection AddComponents(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Person>, PersonRepository>();
            services.AddTransient<IEmailService, EmailService>();
            return services;
        }
    }
}