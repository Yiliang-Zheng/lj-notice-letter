﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Microsoft.Extensions.Options;
using MimeKit;

namespace Data.Infrastructure
{
    public class EmailService:IEmailService
    {
        private readonly ConfigurationSetting _setting;

        public EmailService(IOptions<ConfigurationSetting> options)
        {
            _setting = options.Value;
        }

        public async Task SendAsync(string @from, List<string> to, string content, string subject, params string[] attachments)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(from));
            message.To.AddRange(to.Select(p => new MailboxAddress(p)));
            message.Subject = subject;
            message.Body = GetMessageBody(content, attachments).ToMessageBody();

            using (var stream = new MemoryStream())
            {
                await message.WriteToAsync(stream);
                var credential = new BasicAWSCredentials(this._setting.SesUsername, this._setting.SesPassword);
                using (var client = new AmazonSimpleEmailServiceClient(credential, RegionEndpoint.USWest2))
                {
                    var sendRequest = new SendRawEmailRequest
                    {
                        RawMessage = new RawMessage(stream)
                    };

                    await client.SendRawEmailAsync(sendRequest);
                }
            }
        }

        private BodyBuilder GetMessageBody(string content, params string[] attachments)
        {
            var body = new BodyBuilder
            {
                HtmlBody = content
            };
            if (attachments.Any())
            {
                foreach (var attachment in attachments)
                {
                    body.Attachments.Add(attachment);
                }
            }

            return body;
        }
    }
}