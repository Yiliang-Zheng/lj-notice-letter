﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Infrastructure
{
    public interface IEmailService
    {
        Task SendAsync(string from, List<string> to, string content, string subject, params string[] attachments);
    }
}