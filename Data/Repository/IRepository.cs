﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Entity;

namespace Data.Repository
{
    public interface IRepository<T> where T: EntityBase
    {
        Task<T> GetById(int id);

        Task<T> Get(params object[] parameters);

        Task<List<T>> List(params object[] parameters);

        Task<int> Update(T item);

        Task<int> Insert(T item);

        Task<int> Delete(T item);
    }
}