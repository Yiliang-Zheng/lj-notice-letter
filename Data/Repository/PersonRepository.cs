﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Data.Entity;
using Microsoft.Extensions.Options;

namespace Data.Repository
{
    public class PersonRepository: RepositoryBase, IRepository<Person>
    {
        public PersonRepository(IOptions<ConfigurationSetting> options) : base(options)
        {
        }

        public async Task<Person> GetById(int id)
        {
            using (var connection = this.Connection)
            {
                const string sql = "SELECT PersonId, FirstName, LastName, PersonGuid FROM sec_Person WHERE PersonId = @ID";
                connection.Open();
                var result = await connection.QueryFirstOrDefaultAsync<Person>(sql, new {ID = id});
                return result;
            }
        }

        public Task<Person> Get(params object[] parameters)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<Person>> List(params object[] parameters)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> Update(Person item)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> Insert(Person item)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> Delete(Person item)
        {
            throw new System.NotImplementedException();
        }
    }
}