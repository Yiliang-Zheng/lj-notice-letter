﻿using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace Data.Repository
{
    public abstract class RepositoryBase
    {
        private readonly ConfigurationSetting _db;

        protected RepositoryBase(IOptions<ConfigurationSetting> options)
        {
            this._db = options.Value;
        }

        public IDbConnection Connection => new SqlConnection(this._db.ConnectionString);
    }
}