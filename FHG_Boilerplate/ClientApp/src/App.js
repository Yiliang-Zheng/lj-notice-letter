﻿import React from "react";
import { Route } from "react-router";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Counter from "./components/Counter";
import FetchData from "./components/FetchData";
import AuthorizeRoute from "components/authentication/AuthorizeRoute";
import ApiAuthorizeRoutes from "components/authentication/ApiAuthorizationRoutes";
import { ApplicationPaths } from "utils/authConstants";

export default () => (
  <Layout>
    <Route exact path="/" component={Home} />
    <Route path="/counter" component={Counter} />
    <AuthorizeRoute path="/fetchdata/:startDateIndex?" component={FetchData} />
    <Route
      path={ApplicationPaths.ApiAuthorizationPrefix}
      component={ApiAuthorizeRoutes}
    />
  </Layout>
);
