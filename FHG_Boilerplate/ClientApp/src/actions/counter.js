import { createAction } from "utils/redux";
import types from "actions/types/counter";

export function increment() {
  return function (dispatch) {
    dispatch(createAction(types.INCREMENT_COUNT));
  };
}

export function decrement() {
  return function (dispatch) {
    dispatch(createAction(types.DECREMENT_COUNT));
  };
}
