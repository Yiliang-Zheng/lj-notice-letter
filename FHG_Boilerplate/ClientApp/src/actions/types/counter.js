import { createConstants } from "utils/redux";

const types = createConstants(
  { prefix: "@counter/" },
  "INCREMENT_COUNT",
  "DECREMENT_COUNT"
);

export default types;
