import { createConstants } from "utils/redux";

const types = createConstants(
  { prefix: "@weather/" },
  "REQUEST_WEATHER_FORECASTS",
  "RECEIVE_WEATHER_FORECASTS"
);

export default types;
