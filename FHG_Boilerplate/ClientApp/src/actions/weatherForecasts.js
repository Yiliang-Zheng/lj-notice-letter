import { createAction } from "utils/redux";
import { get } from "utils/request";
import types from "actions/types/weatherForecasts";

export function requestWeatherForecasts(startDateIndex) {
  return function (dispatch, getState) {
    if (startDateIndex === getState().weatherForecasts.startDateIndex) {
      // Don't issue a duplicate request (we already have or are loading the requested data)
      return;
    }

    dispatch(createAction(types.REQUEST_WEATHER_FORECASTS, { startDateIndex }));

    const url = `api/SampleData/WeatherForecasts?startDateIndex=${startDateIndex}`;
    return get(url).then((forecasts) => {
      dispatch(
        createAction(types.RECEIVE_WEATHER_FORECASTS, {
          startDateIndex,
          forecasts,
        })
      );
    });
  };
}
