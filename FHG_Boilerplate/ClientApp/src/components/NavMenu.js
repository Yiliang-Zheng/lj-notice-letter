﻿import React from "react";
import { Link } from "react-router-dom";
import "./NavMenu.css";

import { Menu, Breadcrumb } from "antd";
// import styles from "./styles.module.scss";
export default props => (
  <React.Fragment>
    <Menu mode="horizontal" theme="dark">
      <Menu.Item key="home">
        <Link to="/"> Home</Link>
      </Menu.Item>
      <Menu.Item key="counter">
        <Link to="/counter">Counter</Link>
      </Menu.Item>
      <Menu.Item key="fetch data">
        <Link to="/fetchdata">Fetch Data</Link>
      </Menu.Item>
    </Menu>
  </React.Fragment>
);
