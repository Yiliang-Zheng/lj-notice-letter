﻿import { createReducer } from "utils/redux";
import types from "actions/types/counter";

const initialState = { count: 0 };

const reducer = createReducer(initialState, {
  [types.INCREMENT_COUNT]: (state) => ({ ...state, count: state.count + 1 }),
  [types.DECREMENT_COUNT]: (state) => ({ ...state, count: state.count - 1 }),
});

export default reducer;
