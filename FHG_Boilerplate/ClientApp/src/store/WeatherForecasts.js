﻿import { createReducer } from "utils/redux";
import types from "actions/types/weatherForecasts";

const initialState = { forecasts: [], isLoading: false };

const reducer = createReducer(initialState, {
  [types.REQUEST_WEATHER_FORECASTS]: (state, startDateIndex) => ({
    ...state,
    startDateIndex: startDateIndex,
    isLoading: true,
  }),
  [types.RECEIVE_WEATHER_FORECASTS]: (state, payload) => ({
    ...state,
    startDateIndex: payload.startDateIndex,
    forecasts: payload.forecasts,
    isLoading: false,
  }),
});

export default reducer;
