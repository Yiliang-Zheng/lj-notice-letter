export function createReducer(initialState, reducerMap, baseReducer) {
  return (state = initialState, action = {}) => {
    const reducer = reducerMap[action.type];

    if (reducer) {
      return reducer(state, action.payload);
    }

    return baseReducer ? baseReducer(state, action) : state;
  };
}

export function createAction(type, payload) {
  return { type, payload };
}

export function createConstants(...constants) {
  return constants.reduce((acc, constant) => {
    return Object.assign(acc, {
      [constant]: `${constants[0].prefix || ""}${constant}`,
    });
  }, {});
}
