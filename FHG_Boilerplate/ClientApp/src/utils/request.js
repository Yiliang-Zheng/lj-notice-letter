const UNAUTHORIZED = 401;

function _constructHeaders() {
  return {
    "Content-Type": "application/json",
  };
}

function handleResponse(response) {
  return new Promise((resolve, reject) => {
    const contentType = response.headers.get("content-type");
    if (contentType && contentType.indexOf("application/json") !== -1) {
      response
        .json()
        .then((json) => {
          if (response.ok) {
            resolve(json);
          } else {
            reject(json);
          }
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    } else {
      response
        .text()
        .then((text) => {
          if (response.ok) {
            resolve(text);
          } else {
            reject(text);
          }
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    }
  });
}

export function post(url, payload) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: "POST",
      headers: _constructHeaders(),
      body: JSON.stringify(payload),
    })
      .then((response) => {
        return handleResponse(response);
      })
      .then((json) => {
        resolve(json);
      })
      .catch((error) => {
        console.log(error.message);
        reject(error);
      });
  });
}

export function put(url, payload) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: "PUT",
      headers: _constructHeaders(),
      body: JSON.stringify(payload),
    })
      .then((response) => {
        return handleResponse(response);
      })
      .then((json) => {
        resolve(json);
      })
      .catch((error) => {
        console.log(error.message);
        reject(error);
      });
  });
}

export function get(url) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: "GET",
      headers: _constructHeaders(),
    })
      .then((response) => {
        return handleResponse(response);
      })
      .then((json) => {
        resolve(json);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
}

export function del(url, payload) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: "DELETE",
      headers: _constructHeaders(),
      body: JSON.stringify(payload),
    })
      .then((response) => {
        return handleResponse(response);
      })
      .then((json) => {
        resolve(json);
      })
      .catch((error) => {
        console.log(error.message);
        reject(error);
      });
  });
}
