﻿using System;
using System.Security.Principal;

namespace Identity
{
    public class ApplicationUser : IIdentity
    {
        public virtual int UserId { get; set; }
        public virtual Guid UserGuid { get; set; }
        public virtual string Email { get; set; }

        public virtual string Password { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public string AuthenticationType { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Name { get; set; }
    }
}
